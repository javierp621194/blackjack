const router = require("express").Router();
const path = require("path");

const serveLandingPage = (req, res) => {
  res.sendFile(path.join(__dirname, "../public/index.html"));
};

router.get("/", serveLandingPage);

router.get("/about", serveLandingPage);

module.exports = router;
