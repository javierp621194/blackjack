const {Router} = require("express");
const userRouter = Router();
const apiRouter = Router();
const mongoClient = require("../db/connections");
const db = mongoClient.db("blackjack");

userRouter.get("/", (req, res) => {
    res.send("hello browser");
});

userRouter.post("/", (req, res) => {
    console.log(req.body);
    res.json({weSawTheData: true});
});

apiRouter.use("/user", userRouter);

module.exports = apiRouter;