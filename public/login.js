function noEmptyFields() {
    return registrationFormInputs.every(isNotEmpty);
}

function passwordsMatch() {
    return enterPasswordInput.value === confirmPasswordInput.value;
}

function legalCheckboxChecked() {
    return registrationForm[5].checked;
}

async function registerUser() {
    try {
        const theData = {
            username: usernameInput.value,
            password: passwordInput.value,
            securityQuestion: securityQuestionInput.value,
            securityAnswer: securityAnswerInput.value,
        }
        console.log("the user submitted", theData);
        const response = await fetch("/api/user", {
            method: "POST",
            headers: {
                "Content-Type" : "application/json"
            },
            body: JSON.stringify(theData)
        })
        const resData = await response.json();
        console.log("Success:", resData);
    } catch (err) {
        console.error(err);
    }
}